<?php

namespace Alexssssss\OrmModelBundle\Command;

final class ModelCommand extends \Alexssssss\OrmModel\Console\Command\Make\Model
{
    protected static $defaultName = "ormmodel:make:model";
}
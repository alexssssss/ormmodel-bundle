<?php

namespace Alexssssss\OrmModelBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\Loader\PhpFileLoader;

class OrmModelBundle extends Bundle
{
    public function boot()
    {
        \Alexssssss\OrmModel\Service\ServiceFactory::setDiContainer($this->container);
    }
}
<?php

namespace Alexssssss\OrmModelBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

class OrmModelExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        foreach (array('type', 'server', 'readSlave1', 'readSlave2', 'readSlave3', 'readSlave4', 'readSlave5', 'username', 'password', 'database', 'charset') as $attribute) {
            $container->setParameter('ormModel.connection.' . $attribute, $config['connection'][$attribute]);
        }

        $loader = new Loader\YamlFileLoader(
            $container,
            new FileLocator(__DIR__ . '/../Resources/config')
        );
        $loader->load('services.yaml');
    }
}
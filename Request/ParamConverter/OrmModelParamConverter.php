<?php

namespace Alexssssss\OrmModelBundle\Request\ParamConverter;

use Alexssssss\OrmModel\Entity\EntityInterface;
use Alexssssss\OrmModel\Service\ServiceInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OrmModelParamConverter implements ParamConverterInterface
{
    /**
     * @param Request $request
     * @param ParamConverter $configuration
     *
     * @return bool
     * @throws \Exception
     */
    function apply(Request $request, ParamConverter $configuration): bool
    {
        $name = $configuration->getName();
        $class = $configuration->getClass();
        $options = $this->getOptions($configuration);

        if ($request->attributes->get($name, false) === null) {
            $configuration->setIsOptional(true);
        }


        $errorMessage = null;
        if ($expr = $options['expr']) {
            $object = $this->getOneByExpression($class, $request, $expr, $options, $configuration);

            if ($object === null) {
                $errorMessage = sprintf('The expression "%s" returned null', $expr);
            }

            // find by identifier?
        } elseif (false === $object = $this->getById($class, $request, $options, $name)) {
            // find by criteria
            if (false === $object = $this->getOneBy($class, $request, $options)) {
                if ($configuration->isOptional()) {
                    $object = null;
                } elseif ($options['create_if_no_params'] === true) {
                    $object = $this->create($class, $options);
                } else {
                    throw new \LogicException(sprintf('Unable to guess how to get a OrmModel instance from the request information for parameter "%s".', $name));
                }
            }
        }

        if ($object === null && $configuration->isOptional() === false) {
            $message = sprintf('%s object not found by the @%s annotation.', $class, $this->getAnnotationName($configuration));
            if ($errorMessage) {
                $message .= ' ' . $errorMessage;
            }
            throw new NotFoundHttpException($message);
        }

        $request->attributes->set($name, $object);

        return true;
    }

    protected function create($class, $options)
    {
        $service = $this->getService($options['service'], $class);
        return $service->create();
    }

    protected function getById($class, Request $request, $options, $name)
    {
        if ($options['mapping'] || $options['exclude']) {
            return false;
        }

        $id = $this->getIdentifier($request, $options, $name);

        if (false === $id || null === $id) {
            return false;
        }

        $service = $this->getService($options['service'], $class);
        if ($options['evict_cache'] && $service instanceof ServiceInterface) {
            throw new \Exception('Not supported');
        }

        if ($entity = $service->getOne((int)$id)) {
            return $entity;
        } else {
            return;
        }
    }

    protected function getIdentifier(Request $request, $options, $name)
    {
        if ($options['id'] !== null) {
            if (!is_array($options['id'])) {
                $name = $options['id'];
            } elseif (is_array($options['id'])) {
                $id = [];
                foreach ($options['id'] as $field) {
                    $id[$field] = $request->attributes->get($field);
                }

                return $id;
            }
        }

        if ($request->attributes->has($name)) {
            return $request->attributes->get($name);
        }

        if ($request->attributes->has('id') && !$options['id']) {
            return $request->attributes->get('id');
        }

        return false;
    }

    protected function getOneBy($class, Request $request, $options)
    {
        if (!$options['mapping']) {
            $keys = $request->attributes->keys();
            $options['mapping'] = $keys ? array_combine($keys, $keys) : [];
        }

        foreach ($options['exclude'] as $exclude) {
            unset($options['mapping'][$exclude]);
        }

        if (!$options['mapping']) {
            return false;
        }

        // if a specific id has been defined in the options and there is no corresponding attribute
        // return false in order to avoid a fallback to the id which might be of another object
        if ($options['id'] && null === $request->attributes->get($options['id'])) {
            return false;
        }

        $criteria = [];
        $service = $this->getService($options['service'], $class);

        // todo: replace this for something cached
        /* @var $testEntity \Alexssssss\OrmModel\Entity\EntityInterface */
        $testEntity = $service->create();
        $fields = array_keys($testEntity->toArrayForSave());

        foreach ($options['mapping'] as $attribute => $field) {
            if (in_array($field, $fields)) {
                $criteria[$field] = $request->attributes->get($attribute);
            }
        }

        if ($options['strip_null']) {
            $criteria = array_filter($criteria, function ($value) {
                return null !== $value;
            });
        }

        if (!$criteria) {
            return false;
        }

        if ($entity = $service->getOne($criteria)) {
            return $entity;
        } else {
            return;
        }
    }

    protected function getOneByExpression($class, Request $request, $expression, $options, ParamConverter $configuration)
    {
        throw new \Exception('Not supported');
    }

    /**
     * @param ParamConverter $configuration
     *
     * @return bool
     */
    public function supports(ParamConverter $configuration): bool
    {
        // is it a class?
        if (!$class = $configuration->getClass()) {
            return false;
        }

        // is it an EntityInterface class?
        if (!is_subclass_of($class, EntityInterface::class)) {
            return false;
        }

        $options = $this->getOptions($configuration, false);

        $service = $this->getService($options['service'], $configuration->getClass());
        if ($service === null) {
            return false;
        }

        $return = $service instanceof ServiceInterface;

        return $return;
    }

    protected function getOptions(ParamConverter $configuration, $strict = true)
    {
        $defaultValues = [
            'service' => null,
            'create_if_no_params' => true,
            'exclude' => [],
            'mapping' => [],
            'strip_null' => false,
            'expr' => null,
            'id' => null,
            'evict_cache' => false,
        ];

        $passedOptions = $configuration->getOptions();

        $extraKeys = array_diff(array_keys($passedOptions), array_keys($defaultValues));
        if ($extraKeys && $strict) {
            throw new \InvalidArgumentException(sprintf('Invalid option(s) passed to @%s: %s', $this->getAnnotationName($configuration), implode(', ', $extraKeys)));
        }

        return array_replace($defaultValues, $passedOptions);
    }

    protected function getService($name, $class)
    {
        if ($name === null) {
            $reflection = new \ReflectionClass($class);

            $params = $reflection->getConstructor()->getParameters();
            return ($params[0]->getClass()->name)::getInstance();
        }

        return $this->registry->getService($name);
    }

    protected function getAnnotationName(ParamConverter $configuration)
    {
        $reflection = new \ReflectionClass($configuration);

        return $reflection->getShortName();
    }
}
<?php

namespace Alexssssss\OrmModelBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('orm_model');

        /** @var ArrayNodeDefinition $rootNode */
        $rootNode = method_exists($treeBuilder, 'getRootNode') ? $treeBuilder->getRootNode() : $treeBuilder->root('orm_model');

        $rootNode
            ->children()
                ->arrayNode('connection')
                    ->children()
                        ->scalarNode('type')->defaultValue('')->end()
                        ->scalarNode('server')->defaultValue('')->end()
                        ->scalarNode('readSlave1')->defaultValue('')->end()
                        ->scalarNode('readSlave2')->defaultValue('')->end()
                        ->scalarNode('readSlave3')->defaultValue('')->end()
                        ->scalarNode('readSlave4')->defaultValue('')->end()
                        ->scalarNode('readSlave5')->defaultValue('')->end()
                        ->scalarNode('username')->defaultValue('')->end()
                        ->scalarNode('password')->defaultValue('')->end()
                        ->scalarNode('database')->defaultValue('')->end()
                        ->scalarNode('charset')->defaultValue('utf8')->end()
                    ->end()
                ->end() // connection
            ->end();

        return $treeBuilder;
    }
}
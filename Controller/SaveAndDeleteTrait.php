<?php

namespace Alexssssss\OrmModelBundle\Controller;

use Symfony\Contracts\Translation\TranslatorInterface;

trait SaveAndDeleteTrait
{
    /**
     * @var TranslatorInterface
     */
    public $translator;

    /**
     * @param TranslatorInterface $translator
     * @required
     */
    public function initTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     *
     * @param string $successMsg
     * @param string $errorMsg
     * @return bool True if the entity is successfully saved
     */
    protected function saveEntitiesOrReturnMsgWithFlashMsg(iterable &$entities, $successMsg = null, $errorMsg = null, bool $validate = true, array $args = [])
    {
        try {
            $allSuccessfully = true;

            if ($validate === true) {
                foreach ($entities as $entity) {
                    if ($entity instanceof \Alexssssss\OrmModel\Entity\EntityInterface && $entity->validate() === false) {
                        $allSuccessfully = false;
                    }
                }
            }

            foreach ($entities as $entity) {
                if ($entity instanceof \Alexssssss\OrmModel\Entity\EntityInterface && $entity->save(false, $args) === false) {
                    $allSuccessfully = false;
                }
            }

            if ($allSuccessfully === true) {
                $this->addFlash('success', $successMsg ?? $this->translator->trans('FlashMsg.SaveSuccessfully', [], 'general'));
                return true;
            } else {
                $this->addFlash('warn', $errorMsg ?? $this->translator->trans('FlashMsg.SaveFailed', [], 'general'));
            }
        } catch (\Alexssssss\OrmModel\Exception\Validation $throwable) {
            $this->addFlash('warn', (string)$throwable);
        } catch (\Alexssssss\OrmModel\Exception\Duplicate $throwable) {
            $msg = (string)$throwable;

            if (stripos($msg, "Validation.Duplicate.") !== false) {
                $msg = $this->translator->trans('Validation.Duplicate.Unknown', [], 'general');
            }

            $this->addFlash('warn', $msg);
        }

        return false;
    }

    /**
     * @param \Alexssssss\OrmModel\Entity\EntityInterface $entities
     * @param null $successMsg
     * @param null $errorMsg
     * @return mixed:
     */
    protected function saveEntitiesOrReturnMsgInArray(iterable &$entities, $successMsg = null, $errorMsg = null, bool $validate = true, array $args = [])
    {
        try {
            $allSuccessfully = true;

            if ($validate === true) {
                foreach ($entities as $entity) {
                    if ($entity instanceof \Alexssssss\OrmModel\Entity\EntityInterface && $entity->validate() === false) {
                        $allSuccessfully = false;
                    }
                }
            }

            foreach ($entities as $entity) {
                if ($entity instanceof \Alexssssss\OrmModel\Entity\EntityInterface && $entity->save(false, $args) === false) {
                    $allSuccessfully = false;
                }
            }

            if ($allSuccessfully === true) {
                return ['status' => true, 'msg' => $successMsg ?? $this->translator->trans('FlashMsg.SaveSuccessfully', [], 'general')];
            } else {
                return ['status' => false, 'msg' => $errorMsg ?? $this->translator->trans('FlashMsg.SaveFailed', [], 'general')];
            }
        } catch (\Alexssssss\OrmModel\Exception\Validation $throwable) {
            return ['status' => false, 'msg' => (string)$throwable];
        } catch (\Alexssssss\OrmModel\Exception\Duplicate $throwable) {
            $msg = (string)$throwable;

            if (stripos($msg, "Validation.Duplicate.") !== false) {
                $msg = $this->translator->trans('Validation.Duplicate.Unknown', [], 'general');
            }

            return ['status' => false, 'msg' => (string)$msg];
        }
    }

    /**
     *
     * @param \Alexssssss\OrmModel\Entity\EntityInterface $entity
     * @param string $successMsg
     * @param string $errorMsg
     * @param boolean $validate
     * @param array $args
     * @return boolean True if the entity is successfully saved
     */
    protected function saveEntityOrReturnMsgWithFlashMsg(\Alexssssss\OrmModel\Entity\EntityInterface &$entity, string $successMsg = null, string $errorMsg = null, bool $validate = true, array $args = [])
    {
        try {
            if ($entity->save($validate, $args) !== false) {
                $this->addFlash('success', $successMsg ?? $this->translator->trans('FlashMsg.SaveSuccessfully', [], 'general'));
                return true;
            } else {
                $this->addFlash('warn', $errorMsg ?? $this->translator->trans('FlashMsg.SaveFailed', [], 'general'));
            }
        } catch (\Alexssssss\OrmModel\Exception\Validation $throwable) {
            $this->addFlash('warn', (string)$throwable);
        } catch (\Alexssssss\OrmModel\Exception\Duplicate $throwable) {
            $msg = (string)$throwable;

            if (stripos($msg, "Validation.Duplicate.") !== false) {
                $msg = $this->translator->trans('Validation.Duplicate.Unknown', [], 'general');
            }

            $this->addFlash('warn', $msg);
        }

        return false;
    }

    /**
     *
     * @param \Alexssssss\OrmModel\Entity\EntityInterface $entity
     * @param string $successMsg
     * @param string $errorMsg
     * @param boolean $validate
     * @param array $args
     * @return boolean True if the entity is succesfully saved
     */
    protected function saveAllEntityOrReturnMsgWithFlashMsg(\Alexssssss\OrmModel\Entity\EntityInterface &$entity, $successMsg = null, $errorMsg = null, bool $validate = true, array $args = [])
    {
        try {
            if ($entity->saveAll($validate, $args) !== false) {
                $this->addFlash('success', $successMsg ?? $this->translator->trans('FlashMsg.SaveSuccessfully', [], 'general'));
                return true;
            } else {
                $this->addFlash('warn', $errorMsg ?? $this->translator->trans('FlashMsg.SaveFailed', [], 'general'));
            }
        } catch (\Alexssssss\OrmModel\Exception\Validation $throwable) {
            $this->addFlash('warn', (string)$throwable);
        } catch (\Alexssssss\OrmModel\Exception\Duplicate $throwable) {
            $msg = (string)$throwable;

            if (stripos($msg, "Validation.Duplicate.") !== false) {
                $msg = $this->translator->trans('Validation.Duplicate.Unknown', [], 'general');
            }

            $this->addFlash('warn', $msg);
        }

        return false;
    }

    /**
     * @param \Alexssssss\OrmModel\Entity\EntityInterface $entity
     * @param null $successMsg
     * @param null $errorMsg
     * @param bool $validate
     * @param array $args
     * @return array
     */
    protected function saveEntityOrReturnMsgInArray(\Alexssssss\OrmModel\Entity\EntityInterface &$entity, $successMsg = null, $errorMsg = null, bool $validate = true, array $args = [])
    {
        try {
            if ($entity->save($validate, $args) !== false) {
                return ['status' => true, 'msg' => $successMsg ?? $this->translator->trans('FlashMsg.SaveSuccessfully', [], 'general')];
            } else {
                return ['status' => false, 'msg' => $errorMsg ?? $this->translator->trans('FlashMsg.SaveFailed', [], 'general')];
            }
        } catch (\Alexssssss\OrmModel\Exception\Validation $throwable) {
            return ['status' => false, 'msg' => (string)$throwable];
        } catch (\Alexssssss\OrmModel\Exception\Duplicate $throwable) {
            $msg = (string)$throwable;

            if (stripos($msg, "Validation.Duplicate.") !== false) {
                $msg = $this->translator->trans('Validation.Duplicate.Unknown', [], 'general');
            }

            return ['status' => false, 'msg' => (string)$msg];
        }
    }

    /**
     * @param \Alexssssss\OrmModel\Entity\EntityInterface $entity
     * @param null $successMsg
     * @param null $errorMsg
     * @param bool $validate
     * @param array $args
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    protected function saveEntityOrReturnMsgInJson(\Alexssssss\OrmModel\Entity\EntityInterface &$entity, $successMsg = null, $errorMsg = null, bool $validate = true, array $args = [])
    {
        return $this->json($this->saveEntityOrReturnMsgInArray($entity, $successMsg, $errorMsg, $validate, $args));
    }

    /**
     * @param \Alexssssss\OrmModel\Entity\EntityInterface $entity
     * @param null $successMsg
     * @param null $errorMsg
     * @param bool $validate
     * @param array $args
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    protected function saveEntitiesOrReturnMsgInJson(iterable &$entities, $successMsg = null, $errorMsg = null, bool $validate = true, array $args = [])
    {
        return $this->json($this->saveEntitiesOrReturnMsgInArray($entities, $successMsg, $errorMsg, $validate, $args));
    }

    /**
     *
     * @param \Alexssssss\OrmModel\Entity\EntityInterface $entity
     * @param string $successMsg
     * @param string $errorMsg
     * @param boolean $validate
     * @param array $args
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    protected function saveAllEntityOrReturnMsgInJson(\Alexssssss\OrmModel\Entity\EntityInterface &$entity, string $successMsg = null, string $errorMsg = null, bool $validate = true, array $args = [])
    {
        return $this->json($this->saveAllEntityOrReturnMsgInArray($entity, $successMsg, $errorMsg, $validate, $args));
    }

    /**
     *
     * @param \Alexssssss\OrmModel\Entity\EntityInterface $entity
     * @param string $successMsg
     * @param string $errorMsg
     * @param boolean $validate
     * @param array $args
     * @return array
     */
    protected function saveAllEntityOrReturnMsgInArray(\Alexssssss\OrmModel\Entity\EntityInterface &$entity, string $successMsg = null, string $errorMsg = null, bool $validate = true, array $args = [])
    {
        try {
            if ($entity->saveAll($validate, $args) !== false) {
                return ['status' => true, 'msg' => $successMsg ?? $this->translator->trans('FlashMsg.SaveSuccessfully', [], 'general')];
            } else {
                return ['status' => false, 'msg' => $errorMsg ?? $this->translator->trans('FlashMsg.SaveFailed', [], 'general')];
            }
        } catch (\Alexssssss\OrmModel\Exception\Validation $throwable) {
            return ['status' => false, 'msg' => (string)$throwable];
        } catch (\Alexssssss\OrmModel\Exception\Duplicate $throwable) {
            $msg = (string)$throwable;

            if (stripos($msg, "Validation.Duplicate.") !== false) {
                $msg = $this->translator->trans('Validation.Duplicate.Unknown', [], 'general');
            }

            return ['status' => false, 'msg' => (string)$msg];
        }
    }

    /**
     *
     * @param \Alexssssss\OrmModel\Entity\EntityInterface $entity
     * @param string $successMsg
     * @param string $errorMsg
     * @return boolean True if the entity is succesfully deleted
     */
    protected function deleteEntityOrReturnMsgWithFlashMsg(\Alexssssss\OrmModel\Entity\EntityInterface &$entity, $successMsg = null, $errorMsg = null)
    {
        try {
            if ($entity->delete() !== false) {
                $this->addFlash('success', $successMsg ?? $this->translator->trans('FlashMsg.DeleteSuccessfully', [], 'general'));
                return true;
            } else {
                $this->addFlash('warn', $errorMsg ?? $this->translator->trans('FlashMsg.DeleteFailed', [], 'general'));
            }
        } catch (\Alexssssss\OrmModel\Exception\Validation $throwable) {
            $this->addFlash('warn', (string)$throwable);
        } catch (\Alexssssss\OrmModel\Exception\DeleteForeignKeyConstraint $throwable) {
            $msg = (string)$throwable;

            if (stripos($msg, "Validation.DeleteForeignKeyConstraint.") !== false) {
                $msg = $this->translator->trans('Validation.DeleteForeignKeyConstraint.Unknown', [], 'general');
            }

            $this->addFlash('warn', $msg);
        }

        return false;
    }

    /**
     *
     * @param \Alexssssss\OrmModel\Entity\EntityInterface $entity
     * @param string $successMsg
     * @param string $errorMsg
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    protected function deleteEntityOrReturnMsgInJson(\Alexssssss\OrmModel\Entity\EntityInterface &$entity, $successMsg = null, $errorMsg = null)
    {
        return $this->json($this->deleteEntityOrReturnMsgInArray($entity, $successMsg, $errorMsg));
    }

    /**
     *
     * @param \Alexssssss\OrmModel\Entity\EntityInterface $entity
     * @param string $successMsg
     * @param string $errorMsg
     * @return array
     */
    protected function deleteEntityOrReturnMsgInArray(\Alexssssss\OrmModel\Entity\EntityInterface &$entity, $successMsg = null, $errorMsg = null)
    {
        try {
            if ($entity->delete() !== false) {
                return ['status' => true, 'msg' => $successMsg ?? $this->translator->trans('FlashMsg.DeleteSuccessfully', [], 'general')];
            } else {
                return ['status' => false, 'msg' => $errorMsg ?? $this->translator->trans('FlashMsg.DeleteFailed', [], 'general')];
            }
        } catch (\Alexssssss\OrmModel\Exception\Validation $throwable) {
            return ['status' => false, 'msg' => (string)$throwable];
        } catch (\Alexssssss\OrmModel\Exception\DeleteForeignKeyConstraint $throwable) {
            $msg = (string)$throwable;

            if (stripos($msg, "Validation.DeleteForeignKeyConstraint.") !== false) {
                $msg = $this->translator->trans('Validation.DeleteForeignKeyConstraint.Unknown', [], 'general');
            }

            return ['status' => false, 'msg' => (string)$msg];
        }
    }
}